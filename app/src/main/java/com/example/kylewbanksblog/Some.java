package com.example.kylewbanksblog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Some {

    @SerializedName("object_or_array")
    @Expose
    public String objectOrArray;
    @SerializedName("empty")
    @Expose
    public Boolean empty;
    @SerializedName("parse_time_nanoseconds")
    @Expose
    public Integer parseTimeNanoseconds;
    @SerializedName("validate")
    @Expose
    public Boolean validate;
    @SerializedName("size")
    @Expose
    public Integer size;

    @Override
    public String toString() {
        return "Some{" +
                "objectOrArray='" + objectOrArray + '\'' +
                ", empty=" + empty +
                ", parseTimeNanoseconds=" + parseTimeNanoseconds +
                ", validate=" + validate +
                ", size=" + size +
                '}';
    }
}