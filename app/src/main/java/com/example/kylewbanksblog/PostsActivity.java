package com.example.kylewbanksblog;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class PostsActivity extends Activity {
	
	private static final String TAG = "PostsActivity";

	TextView textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_posts);

		textView = (TextView) findViewById(R.id.textView);
		
		PostFetcher fetcher = new PostFetcher();
		fetcher.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.posts, menu);
		return true;
	}
	

	
	private class PostFetcher extends AsyncTask<Void, Void, Some> {
		private static final String TAG = "PostFetcher";
		//public static final String SERVER_URL = "http://kylewbanks.com/rest/posts.json";
		//public static final String SERVER_URL = "https://jsonplaceholder.typicode.com/photos/1";
		public static final String SERVER_URL = "http://validate.jsontest.com/?json=%7B%22key%22:%22value%22%7D";




		@Override
		protected Some doInBackground(Void... params) {
			try {
				//Create an HTTP client
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(SERVER_URL);
				
				//Perform the request and check the status code
				HttpResponse response = client.execute(post);
				StatusLine statusLine = response.getStatusLine();
				if((statusLine.getStatusCode() >= 200) && (statusLine.getStatusCode() < 300)) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					
					try {
						//Read the server response and attempt to parse it as JSON
						Reader reader = new InputStreamReader(content);
						
						GsonBuilder gsonBuilder = new GsonBuilder();

						Gson gson = gsonBuilder.create();

						Some some = gson.fromJson(reader, Some.class);

						content.close();


						return some;

					} catch (Exception ex) {
						Log.e(TAG, "Failed to parse JSON due to: " + ex);
					}
				} else {
					Log.e(TAG, "Server responded with status code: " + statusLine.getStatusCode());
				}
			} catch(Exception ex) {
				Log.e(TAG, "Failed to send HTTP POST request due to: " + ex);
			}
			return null;

		}


		@Override
		protected void onPostExecute(Some s) {
			super.onPostExecute(s);
			//Toast.makeText(PostsActivity.this, s.objectOrArray, Toast.LENGTH_SHORT).show();
			textView.setText(s.toString());
		}
	}



}
